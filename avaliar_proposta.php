<?php
    require_once 'php/utils/verify_session.php';
    require_once 'php/connection.php';
    require_once 'php/db_utils/getUserInformation.php';
    $user = $_SESSION["user"];
    $userid = getUserId($user,$conn);
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css">
    <link href="bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="css/header.css">
    <link href="css/estilo.css" rel="stylesheet">
    <title>Avaliar Proposta</title>
</head>
<body>
    <?php require_once 'php/header1.php';?>
    <h1  class="alert alert-light p-4" style="font-weight: 200; margin-top:10vh;">Avaliar Proposta</h1>
    <div class="p-4">
        <div id="container" class="mt-5">
    
        </div>
    </div>

    <?php
       
        include_once "php/footer.php";    
    ?>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script>window.jQuery || document.write('<script src="../assets/js/vendor/jquery-slim.min.js"><\/script>')</script>
    <script src="bootstrap/assets/js/vendor/popper.min.js"></script>
    <script src="bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    <script>
       $(document).ready(()=>{
           $.ajax({
               method:'GET',
               url:'php/produto/getPropostas.php',
               data:{user_id:<?=$userid?>},
               success:(resp) => {
                   let propostas = JSON.parse(resp);
                   console.log(propostas);
                   
                   if(propostas.length === 0 ){
                       $("#container")
                       .append(`<div class="alert alert-warning m-4">Não houve escolhas dos utilizadores-ofertantes nos produtos de seu interesse</div>`);
                   }
                   else{
                        //Iterando por par de propostas
                        for(let i = 0; i < propostas.length; i+=2){
                            $("#container")
                            .append(`<div id="${i}" class="row d-flex justify-content-around">`);

                            $(`#${i}`).append(`<div id="n${i}" class="card" style="width:18rem;">`);
                            
                            $(`#n${i} `)
                            .append(`<img class="card-img-top" src=${propostas[i].img_path}>`);

                            $(`#n${i}`).append(`<div class="card-body">`);
                            
                            $(`#n${i} >.card-body`)
                            .append(`<h5 class="card-title ml-3">Nome do produto:</h5>`)
                            .append(`<p class="card-text ml-3">${propostas[i].product_name}</p>`);



                            $(`#${i}`).append(`<div id="n${i+1}" class="card"  style="width:18rem;">`);
                            
                            $(`#n${i+1} `)
                            .append(`<img class="card-img-top" src=${propostas[i + 1].img_path}>`);

                            $(`#n${i+1}`).append(`<div class="card-body">`);
                            
                            $(`#n${i+1} >.card-body`)
                            .append(`<h5 class="card-title ml-3">Nome do produto:</h5>`)
                            .append(`<p class="card-text ml-3">${propostas[i + 1].product_name}</p>`);

                            $("#container")
                            .append(`<div id="b${i}" class="d-flex justify-content-between mt-5">`)

                            $(`#b${i}`)
                            .append('<form id="rejeitar" method="POST" action="php/oferta/rejeitar_proposta.php">');
                            
                            $("#rejeitar")
                            .append(`<input type="hidden" name="oferta" value="${propostas[i].product_id}">`);
                            
                            $("#rejeitar")
                            .append(`<input type="hidden" name="interesse" value="${propostas[i + 1].product_id}">`)
                            
                            $("#rejeitar")
                            .append(`<button type="submit" class="btn btn-outline-danger" style="margin-left:14vw;">Rejeitar</button>`);

                            
                            
                            $(`#b${i}`)
                            .append('<form id="aceitar" method="POST" action="php/oferta/aceitar_proposta.php">');
                            
                            $("#aceitar")
                            .append(`<input type="hidden" name="oferta" value="${propostas[i].product_id}">`);
                            
                            $("#aceitar")
                            .append(`<input type="hidden" name="interesse" value="${propostas[i + 1].product_id}">`)
                            
                            $("#aceitar")
                            .append(`<button type="submit" class="btn btn-outline-primary" style="margin-right:14vw;">Aceitar</button>`);
                            
                        }
                   }
               },
               error: err => console.log(err)
           });
       });
    </script>
</body>
</html>

