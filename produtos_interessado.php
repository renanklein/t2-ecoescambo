<?php
    require_once 'php/utils/verify_session.php';
    require_once 'php/utils/mensagens.php';
    require 'php/produto/produto_display.php';
    require_once 'php/db_utils/getUserInformation.php';
    $username = filter_input(INPUT_GET,"username",FILTER_SANITIZE_STRING);
    $oferta_id = intval(filter_input(INPUT_GET,"oferta_id",FILTER_SANITIZE_STRING));
    $_SESSION["oferta_id"] = $oferta_id;
    
    $query = $conn->prepare("SELECT userid FROM user WHERE username = :user");
    $query->bindParam(':user',$username,PDO::PARAM_STR,60);
    $query->execute();
    $user = $query->fetch(PDO::FETCH_ASSOC);
    $userid = $user['userid'];
    $pagina = filter_input(INPUT_GET,'pagina',FILTER_SANITIZE_NUMBER_INT) !== null ?
    filter_input(INPUT_GET,'pagina',FILTER_SANITIZE_NUMBER_INT) : 1;
    $produtos = getProducts($conn,$pagina,0,$userid,1);
?>
<!DOCTYPE html>
<html >
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css">
    <link href="bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="css/header.css">
    <link href="css/estilo.css" rel="stylesheet">
    <title>Produtos do interessado</title>
</head>
<body>
    <?php require_once 'php/header1.php';?>

    <h1  class="alert alert-light p-4" style="font-weight: 200; margin-top:10vh;">Produtos do interessado</h1>
    <main>
        <div class="d-flex p-4 bg-light">
            <div class="row mt-5 justify-content-center">
                <?php displayProducts($produtos,count($produtos));?>
            </div>
        </div>
        <hr>
        <div class="d-flex justify-content-center">
            <a id="sem_inte" href="php/oferta/retirar_interesse.php" class="mb-2">Não tenho interesse por nenhum</a>
        </div>
        <!--<div class="d-flex justify-content-center align-items-center">
          <ul class="pagination pagination-sm">
            
          </ul>
      </div>-->
    </main>
    <?php require_once 'php/footer.php';?>

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script>window.jQuery || document.write('<script src="../assets/js/vendor/jquery-slim.min.js"><\/script>')</script>
    <script src="bootstrap/assets/js/vendor/popper.min.js"></script>
    <script src="bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    <script>
        $(document).ready(()=>{
            $(".card .card-body .btn").text("Propor Troca por Este");
            $(".card").each(function(){
                $(this).find(".card-body .btn").attr('href',`confirmacao_proposta.php?id=${$(this).find("input").val()}`);
            })
        });
    </script>
</body>
</html>