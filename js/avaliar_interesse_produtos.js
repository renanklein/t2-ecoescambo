 $(document).ready(()=>{
            $.ajax({
                method:'GET',
                url:'php/produto/getProductInteresse.php',
                success:(resp)=>{
                    let produtos = JSON.parse(resp);
                    console.log(produtos);
                    if(produtos.length === 0){
                        $('.row').append(
                            `<div class="alert alert-warning m-4">Ninguem manifestou interesse em seus produtos</div>`
                        );
                    }else{
                        produtos.forEach((item,index)=>{
                            $('.row').append(`<div id = ${index} class="card" style="width:25rem;">`)
                            
                            $(`#${index}`)
                            .append(`<img class="card-img-top" src=${item.img_path}>`)

                            $(`#${index}`)
                            .append(`<div class="card-body">`);
                            
                            $(`#${index} > .card-body`)
                            .append(`<h5 class="card-title ml-3">Nome do produto:</h5>`)
                            .append(`<p class="card-text ml-3">${item.product_name}</p>`)
                            .append(`<h5 class="card-title ml-3">Nome do interessado:</h5>`)
                            .append(`<p class="card-text ml-3">${item.username}</p>`);
                            $(`#${index}`).append('<hr class="mx-4">');

                            $(`#${index}`)
                            .append(`<form method="get" action="produtos_interessado.php">`)
                            
                            $(`#${index} > form`)
                            .append(`<input type="hidden" name="username" value="${item.username}">`)
                            .append(`<input type="hidden" name="oferta_id" value="${item.oferta_id}">`)
                            .append(`<button type="submit" class="btn btn-success float-right mr-4 mb-3">Avaliar</button>`);
                            
                        })
                    }
                },
                error:(err) => console.log(err)
            });
        });