const validationFeedback = (seletor) =>{
    if($(seletor).hasClass('is-invalid')){
        $(seletor).next().remove();
        $(seletor).parent('.form-group').append('<div class="invalid-feedback">Campo preenchido incorretamente</div>');
    }else if($(seletor).hasClass('is-valid')){
        $(seletor).next().remove();
        $(seletor).parent('.form-group').append('<div class="valid-feedback">Campo OK</div>');
    }
}
$(document).ready(() => {
    validationFeedback('#username');
    validationFeedback('#pass');
    validationFeedback('#confirmPass');
    validationFeedback('#email');
    validationFeedback('#tel');
    validationFeedback('#cep');
})
