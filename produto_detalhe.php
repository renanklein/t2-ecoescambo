<?php
    require_once 'php/connection.php';
    require 'php/produto/produto_paginacao.php';
    require_once 'php/utils/verify_session.php';
?>


<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css">
    <link href="bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="css/header.css">
    <link href="css/estilo.css" rel="stylesheet">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Detalhes do produto</title>
</head>
<body>
    <?php
        $id = filter_input(INPUT_GET,'id',FILTER_SANITIZE_NUMBER_INT); 
        $produto = getProduct($conn,$id);
    ?>
    <?php require_once 'php/header1.php';?>
    <?php require_once 'php/header2.php';?>
    <main class="d-flex justify-content-center">
        <?php require_once 'html/produto_detalhe.inc';?>
    </main>
    <?php require_once 'php/footer.php';?>
</body>

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script>window.jQuery || document.write('<script src="../assets/js/vendor/jquery-slim.min.js"><\/script>')</script>
<script src="bootstrap/assets/js/vendor/popper.min.js"></script>
<script src="bootstrap/dist/js/bootstrap.min.js"></script>
</html>