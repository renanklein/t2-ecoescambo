<?php
  session_start();
  require_once 'php/utils/mensagens.php';
  require 'php/produto/produto_display.php';
?>
<!doctype html>
<html lang="pt-br">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="icon" href="bootstrap/favicon.ico">

    <title>EcoEscambo</title>

    <!-- Principal CSS do Bootstrap -->
    <link href="bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="css/estilo.css" rel="stylesheet">

    <!-- Estilos customizados para esse template -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css">
    <link rel="stylesheet" href="css/header.css">
  </head>

  <body>
    <?php
        include_once "php/header1.php";
        include_once "php/header2.php";  
        
        $pagina = filter_input(INPUT_GET,'pagina',FILTER_SANITIZE_NUMBER_INT) !== null ?
        filter_input(INPUT_GET,'pagina',FILTER_SANITIZE_NUMBER_INT) : 1;

        $produtos;
        if(isset($_SESSION["user"])){
          $query = $conn->prepare("SELECT userid FROM user WHERE username = :uname");
          $query->bindParam(':uname',$_SESSION['user'],PDO::PARAM_STR,60);
          $query->execute();
          $user = $query->fetch(PDO::FETCH_ASSOC);
          $produtos = getProducts($conn,$pagina,10,$user['userid'],3);
        }else{
          $produtos = getProducts($conn,$pagina,10,'',3);
        }  
    ?>



    <main role="main">
      <?php exibeMensage('success');?>
      <?php exibeMensage('msg');?>

      <div class="d-flex p-4 bg-light">
          <div class="row mt-5 justify-content-center ">
            <?php count($produtos) === 0 ? '<div class="alert alert-danger">Não há produtos para serem exibidos</div>':
                displayProducts($produtos,count($produtos)); ?>
          </div>
      </div>
      <hr>
      <div class="d-flex justify-content-center align-items-center">
          <ul class="pagination pagination-sm">
              <?php require 'php/produto/button_display.php';?>
          </ul>
      </div>
    
    </main> 

    <?php
       
        include_once "php/footer.php";    
    ?>
    

    <!-- Principal JavaScript do Bootstrap
    ================================================== -->
    <!-- Foi colocado no final para a página carregar mais rápido -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script>window.jQuery || document.write('<script src="../assets/js/vendor/jquery-slim.min.js"><\/script>')</script>
    <script src="bootstrap/assets/js/vendor/popper.min.js"></script>
    <script src="bootstrap/dist/js/bootstrap.min.js"></script>
    
  </body>
</html>
