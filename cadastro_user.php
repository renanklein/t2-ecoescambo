<?php
    session_start();
    function exibeError(){
    if(isset($_SESSION["msg"])){
        $erro = $_SESSION["msg"];
        echo "$erro";
        unset($_SESSION["msg"]);
    }
   }
   function errorClass($index){
       if(isset($_SESSION['erroClass'])){
           $erro = $_SESSION['erroClass'];
           echo "$erro[$index]";
       }
   }
?>

<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!-- Principal CSS do Bootstrap -->
    <link href="bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <title>Cadastro de usuário</title>
</head>

<body >
    <?php require_once 'php/header1.php';?>
    <?php require_once 'html/cadastro_user.html';?>
    <?php require_once 'php/footer.php';?>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="../assets/js/vendor/jquery-slim.min.js"><\/script>')</script>
    <script src="bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.15/jquery.mask.min.js"></script>
    <script>
       $(document).ready(()=>{
            $.ajax({
                url: 'js/validation.js',
                dataType:'script',
                success: () => console.log('carregou o arquivo js')
            });
            $("#tel").mask("(00) 00000-0000");
            $("#cep").mask("00000-000");
       })
       
    </script>
    
</body>

</html>
