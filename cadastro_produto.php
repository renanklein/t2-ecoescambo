<?php
    session_start();
    if(!isset($_SESSION["user"])){
        $_SESSION["msg"] = '<p class="alert alert-danger">Acesso Negado! Se autentique no sistema para ter acesso ao conteúdo</p>';
        header('Location: indexecoescambo.php');
    }
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css">
    <link rel="stylesheet" href="css/cadastro_produto.css">
    <link rel="stylesheet" href="css/header.css">

    <title>Cadastro de produto</title>
</head>
<body>
    <?php require_once "php/header1.php";?>
    <?php require_once "html/cadastro_product.html";?>
    <?php require_once "php/footer.php";?>

    <!-- Principal JavaScript do Bootstrap
    ================================================== -->
    <!-- Foi colocado no final para a página carregar mais rápido -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script>window.jQuery || document.write('<script src="../assets/js/vendor/jquery-slim.min.js"><\/script>')</script>
    <script src="bootstrap/assets/js/vendor/popper.min.js"></script>
    <script src="bootstrap/dist/js/bootstrap.min.js"></script>
</body>
</html>