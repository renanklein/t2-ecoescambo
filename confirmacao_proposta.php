<?php
    require_once 'php/utils/verify_session.php';
    require_once 'php/utils/mensagens.php';
    require_once 'php/connection.php';
    $oferecido_id = filter_input(INPUT_GET,"id",FILTER_SANITIZE_NUMBER_INT);
    $oferta_id = $_SESSION['oferta_id'];
    $query = $conn->prepare("SELECT produto_ofertado_id FROM oferta WHERE oferta_id = :id");
    $query->bindParam(':id',$oferta_id,PDO::PARAM_INT);
    $query->execute();
    $produto = $query->fetch(PDO::FETCH_ASSOC);
    $ofertado_id = $produto['produto_ofertado_id'];
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css">
    <link href="bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="css/header.css">
    <link href="css/estilo.css" rel="stylesheet">
    <title>Confirmação proposta</title>
</head>
<body>
    <?php require_once 'php/header1.php';?>
    <h1  class="alert alert-light p-4" style="font-weight: 200; margin-top:10vh;">Confirmar Proposta</h1>
    <div class="p-4  bg-light">
        <div class="row mt-5 d-flex justify-content-around">

        </div>
    </div>
        <hr>
        <div class="d-flex justify-content-around">
            <form method="GET" action="php/oferta/cadastrar_oferecido.php">
                <input type="hidden" name="produto" value="<?=$oferecido_id?>">
                <button type="submit" class="btn btn-outline-primary">Enviar</button>
            </form>
        </div>
        <?php
       
        include_once "php/footer.php";    
        ?>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script>window.jQuery || document.write('<script src="../assets/js/vendor/jquery-slim.min.js"><\/script>')</script>
    <script src="bootstrap/assets/js/vendor/popper.min.js"></script>
    <script src="bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    <script>
        $(document).ready(()=>{
            let ids = JSON.stringify([<?=$ofertado_id?>,<?=$oferecido_id?>]);
            $.ajax({
                method:'GET',
                url:'php/produto/getProduct.php',
                data:{
                    produtos_id: ids,
                },
                success:(resp)=>{
                    let produtos = JSON.parse(resp);
                    produtos.forEach((item,index)=>{
                            $('.row').append(`<div id = ${index} class="card" style="width:25rem;">`)
                            
                            $(`#${index}`)
                            .append(`<img class="card-img-top" src=${item.img_path}>`)

                            $(`#${index}`)
                            .append(`<div class="card-body">`);
                            
                            $(`#${index} > .card-body`)
                            .append(`<h5 class="card-title ml-3">Nome do produto:</h5>`)
                            .append(`<p class="card-text ml-3">${item.product_name}</p>`)
                            
                        })
                },
                error: err => console.log(err)
            });
        });
    </script>
</body>
</html>