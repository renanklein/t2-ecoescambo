<?php
    require_once 'php/utils/verify_session.php';
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Confirmação de interesse</title>
    
    <!-- Principal CSS do Bootstrap -->
    <link href="bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="css/estilo.css" rel="stylesheet">
    <!-- Estilos customizados para esse template -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css">
    <link rel="stylesheet" href="css/header.css">

</head>
<body>
    <?php require_once 'php/header1.php';?>
    <main class="d-flex flex-column justify-content-center bg-light" style="margin-top:30vh;">
        <div class="alert alert-success m-5 p-5">Seu interesse foi registrado com sucesso. Aguarde o contato do ofertante</div>
        <div class="d-flex mb-2 justify-content-center">
            <a href="indexecoescambo.php" class="mt-2">Voltar</a>
        </div>
    </main>

    <div class="d-flex flex-column justify-content-end">
        <?php include_once 'php/footer.php';?>
    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="../assets/js/vendor/jquery-slim.min.js"><\/script>')</script>
    <script src="bootstrap/assets/js/vendor/popper.min.js"></script>
    <script src="bootstrap/dist/js/bootstrap.min.js"></script>
</body>
</html>