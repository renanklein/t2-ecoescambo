<main class="m-5">
        <div class="card" style="width: 25rem;">
            <img src="<?=$produto->getImgPath();?>" alt="" class="card-img-top">
            <div class="card-body">
                <h5 class="card-title">Nome:</h5>
                <p class="card-text">
                    <?=$produto->getProductName()?>
                </p>
                <h5 class="card-title mt-2">Descrição do produto:</h5>
                <p class="card-text">
                    <?=$produto->getIndexDesc();?>
                </p>

                <h5 class="card-title mt-2">Descrição da troca:</h5>
                <p class="card-text">
                  <?=$produto->getDetalhes();?>
                </p>

                <hr>

                <a href="indexecoescambo.php" class="float-left ml-2 mt-2">Voltar</a>
                <a href="php/oferta/cadastrar_interesse.php?id=<?=$produto->getId()?>" class="btn btn-outline-primary float-right mr-2">Confirmar</a>
            </div>
        </div>
    </main>