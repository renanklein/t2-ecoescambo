<?php
    //Página de exibição do contato do ofertando que criou a oferta após o arremate
    require_once 'php/utils/verify_session.php';
    require_once 'php/connection.php';
    require_once 'php/utils/mensagens.php';
    $product_id = intval(filter_input(INPUT_GET,"ofertado",FILTER_SANITIZE_NUMBER_INT));

    $query = $conn->prepare("SELECT user_userid FROM product WHERE product_id = :id");
    $query->bindParam(":id",$product_id,PDO::PARAM_INT);

    $query->execute();

    $userid = $query->fetch(PDO::FETCH_ASSOC);

    $query = $conn->prepare("SELECT email, tel FROM user WHERE userid = :uid");

    $query->bindParam(":uid",$userid["user_userid"],PDO::PARAM_INT);
    
    $query->execute();

    $contatos = $query->fetch(PDO::FETCH_ASSOC);

?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <!-- Principal CSS do Bootstrap -->
    <link href="bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="css/estilo.css" rel="stylesheet">
    <!-- Estilos customizados para esse template -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css">
    <link rel="stylesheet" href="css/header.css">
    <title>Contatos</title>
</head>
<body>    

    <?php include_once "php/header1.php";?>

    <main>
        <?php exibeMensage('success');?>
        <?php exibeMensage('msg');?>

        <div class="jumbotron p-4" style="margin:10vw 10vh;">
            <h1 class="display-4">Contatos</h1>
            <p class="lead">E-mail: <?=$contatos["email"]?></p>
            <p class="lead">Telefone: <?=$contatos["tel"]?></p>
            <hr>
            <a href="indexecoescambo.php" class="ml-2">Voltar</a>
        </div>
    </main>
    <?php include_once 'php/footer.php';?>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="../assets/js/vendor/jquery-slim.min.js"><\/script>')</script>
    <script src="bootstrap/assets/js/vendor/popper.min.js"></script>
    <script src="bootstrap/dist/js/bootstrap.min.js"></script>
</body>
</html>
