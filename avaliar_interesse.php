<?php
    require_once 'php/utils/verify_session.php';
    require_once 'php/utils/mensagens.php';
    require 'php/produto/produto_display.php';
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!-- Principal CSS do Bootstrap -->
    <link href="bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="css/estilo.css" rel="stylesheet">
    <!-- Estilos customizados para esse template -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css">
    <link rel="stylesheet" href="css/header.css">
    <title>Avaliar Interesse</title>
</head>
<body>
    <?php
        include_once "php/header1.php"; 
    ?>
     <h1 class="alert alert-light p-4" style="font-weight: 200; margin-top:10vh;">Avaliar Interesse</h1>

      <main>
        <div class="d-flex p-4 bg-light">
            <div class="row mt-5 justify-content-center ">
           
             </div>
        </div>
        <hr>
        <div class="d-flex justify-content-center align-items-center">
            <ul class="pagination pagination-sm">
                <?php require 'php/produto/button_display.php';?>
            </ul>
        </div>
      </main>
    <?php include_once 'php/footer.php';?>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="../assets/js/vendor/jquery-slim.min.js"><\/script>')</script>
    <script src="bootstrap/assets/js/vendor/popper.min.js"></script>
    <script src="bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="js/avaliar_interesse_produtos.js"></script>
</body>
</html>