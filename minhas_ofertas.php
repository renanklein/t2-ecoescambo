<?php
  require_once 'php/utils/verify_session.php';
  require_once 'php/utils/mensagens.php';
  require 'php/produto/produto_display.php';
  $username = $_SESSION["user"];
  $result = $conn->prepare("SELECT userid FROM user WHERE username  = :user;");
  $result->bindParam(':user',$username,PDO::PARAM_STR,60);
  $result->execute();
  $user = $result->fetch(PDO::FETCH_ASSOC);
  $userid = $user['userid'];
  $pagina = filter_input(INPUT_GET,'pagina',FILTER_SANITIZE_NUMBER_INT) !== null ?
        filter_input(INPUT_GET,'pagina',FILTER_SANITIZE_NUMBER_INT) : 1;
          
  $produtos = getProducts($conn,$pagina,10,$userid,2);
?>
<!doctype html>
<html lang="pt-br">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="icon" href="bootstrap/favicon.ico">

    <title>EcoEscambo</title>

    <!-- Principal CSS do Bootstrap -->
    <link href="bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="css/estilo.css" rel="stylesheet">
    <!-- Estilos customizados para esse template -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css">
    <link rel="stylesheet" href="css/header.css">
  </head>
  <body>
    <?php
        include_once "php/header1.php"; 
    ?>
    <main>
        <h1 class="alert alert-light p-4" style="font-weight: 200; margin-top:10vh;"> Minhas Ofertas</h1>
      <div class="d-flex p-4 bg-light">
          <div class="row mt-5 justify-content-center ">
            <?php displayProducts($produtos,count($produtos)); ?>
          </div>
      </div>
      <hr>
      <div class="d-flex justify-content-center align-items-center">
          <ul class="pagination pagination-sm">
              <?php require 'php/produto/button_display.php';?>
          </ul>
      </div>
    </main>
    <?php include_once 'php/footer.php';?>


    <!-- Principal JavaScript do Bootstrap
    ================================================== -->
    <!-- Foi colocado no final para a página carregar mais rápido -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="../assets/js/vendor/jquery-slim.min.js"><\/script>')</script>
    <script src="bootstrap/assets/js/vendor/popper.min.js"></script>
    <script src="bootstrap/dist/js/bootstrap.min.js"></script>
    <script>
       $(document).ready(()=>{
          $(".card .card-body .btn").hide();
          $.ajax({
            method:'GET',
            url:'php/produto/getStatus.php',
            success:(resp) =>{
              let dados = JSON.parse(resp);
              console.log(dados);
              $(".card .card-body").each((index,elem)=>{
                $(elem).children('.card-text').show();
                let status = '';
                dados[index].status === "Em aberto" ? 
                  status = `<p class="text-success mt-2">${dados[index].status}</p>`:
                  status = `<p class="text-danger mt-2">${dados[index].status}</p>`;
                  $(elem).append(status);
              });
            },
            error:(err) =>console.log(err)
          });
      });
    </script>
  </body>
</html>