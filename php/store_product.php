<?php
    session_start();
    require 'connection.php';
    if(isset($_FILES["imagem"])){
        $user = $_SESSION["user"];
        $result = $conn->query("SELECT userid from user WHERE username = '$user'");
        $userBD = $result->fetch(PDO::FETCH_ASSOC);
        $id = $userBD["userid"];

        $nome_produto = filter_input(INPUT_POST,"nome",FILTER_SANITIZE_STRING);
        $imagem_produto = $_FILES["imagem"];
        $descricao_inicial = filter_input(INPUT_POST,"descricao",FILTER_SANITIZE_STRING);
        $detalhes = filter_input(INPUT_POST,"detalhes",FILTER_SANITIZE_STRING);

        move_uploaded_file($_FILES["imagem"]["tmp_name"],'../img/'.$_FILES["imagem"]['name']);
        $img_path = "img/".$_FILES["imagem"]['name'];

        $sql = $conn->prepare(
            "INSERT INTO product (img_path,product_name,index_description,detail_description,user_userid) VALUES (:img_path,:nome,:descricao,:detalhes,:id);"
        );
        $sql->bindParam(":img_path",$img_path,PDO::PARAM_STR,255);
        $sql->bindParam(":nome",$nome_produto,PDO::PARAM_STR,40);
        $sql->bindParam(":descricao",$descricao_inicial,PDO::PARAM_LOB);
        $sql->bindParam(":detalhes",$detalhes,PDO::PARAM_LOB);
        $sql->bindParam(":id",$id,PDO::PARAM_INT);

        $sql->execute();

        $result = $conn->query("SELECT MAX(product_id) as id from product");
        $productDB = $result->fetch(PDO::FETCH_ASSOC);
        $productid = $productDB["id"];
        $status = 'Em aberto';
        $sql = $conn->prepare(
            "INSERT INTO oferta (ofertante_id,produto_ofertado_id,status) VALUES (:usid,:pid,:status);"
        );
        $sql->bindParam(":usid",$id,PDO::PARAM_INT);
        $sql->bindParam(":pid",$productid,PDO::PARAM_INT);
        $sql->bindParam(":status",$status,PDO::PARAM_STR);
        $sql->execute();

        $_SESSION["success"] = '<div class="alert alert-success m-4 p-4">Produto cadastrado com sucesso !</div>';
        header('location: ../indexecoescambo.php');
    }
    else{
        $_SESSION["msg"] = '<div class="alert alert-danger m-4 p-4">Ocorreu um erro ao cadastrar um novo produto </div>';
        header('location: ../indexecoescambo.php');
    }


