<?php
   function getUserId($username,$conn){
        $result = $conn->prepare("SELECT userid FROM user WHERE username  = :user;");
        $result->bindParam(':user',$username,PDO::PARAM_STR,60);
        $result->execute();
        $user = $result->fetch(PDO::FETCH_ASSOC);
        $userid = $user['userid'];

        return $userid;
    }