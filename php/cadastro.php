<?php 
session_start();
require_once 'connection.php';
require_once 'utils/cadastro_db.php';
if(isset($_SESSION['erroClass'])){
    unset($_SESSION['erroClass']);
}

//Nesse arquivo ocorre inserção no banco de dados e validação dos dados inseridos
require_once 'utils/cadastro_validation.php';
$validArray = validateCadastro($user,$password,$confirmPass,$email,$tel,$cep);

if(in_array('is-invalid',$validArray)){
    onValidateError($validArray);
}
else{
    $result = ifUserExistsDB($conn);
    if($result->rowCount() > 0){
        $userDB = $result->fetchAll();
        $validFlag = true;
        foreach($userDB as $userRow){
            if($userRow["username"] == $user){
                userAlreadyExists();
                $validFlag = false;
             } 
             else if($userRow['email'] == $email){
                 emailAlreadyExists();
                 $validFlag = false;
             }
             else if($userRow['tel'] == $tel){
                phoneAlreadyExists();
                $validFlag = false;
            } 
        }
        if($validFlag){
            userSignUp($user, $password, $email, $tel, $cep, $conn);
        }
    }
    else{
        userSignUp($user, $password, $email, $tel, $cep, $conn);
    }
}





    




