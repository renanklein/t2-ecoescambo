<?php
    require_once '../connection.php';
    session_start();
    $pagina = filter_input(INPUT_GET,'pagina',FILTER_SANITIZE_NUMBER_INT) !== null ?
    filter_input(INPUT_GET,'pagina',FILTER_SANITIZE_NUMBER_INT) : 1;
    $limite = 10;

    $primeiro = ($pagina - 1)*$limite;

    $username = $_SESSION['user'];

    $result  = $conn->prepare("SELECT userid FROM user WHERE username = :user");
    $result->bindParam(':user',$username,PDO::PARAM_STR,60);
    $result->execute();
    $user = $result->fetch(PDO::FETCH_ASSOC);

    $userid = $user["userid"];

    $result = $conn->prepare(
        "SELECT produto_ofertado_id,status FROM oferta WHERE ofertante_id = :id ORDER BY produto_ofertado_id LIMIT :primeiro,:limite"
    );
    $result->bindParam(':id',$userid,PDO::PARAM_INT);
    $result->bindParam(':primeiro',$primeiro,PDO::PARAM_INT);
    $result->bindParam(':limite',$limite,PDO::PARAM_INT);
    $result->execute();
    $oferta = $result->fetchAll(PDO::FETCH_ASSOC);

    echo(json_encode($oferta));

