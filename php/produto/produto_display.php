<?php 
    require_once 'php/produto/produto.php';
    require 'php/produto/produto_paginacao.php'; 
    require_once 'php/connection.php';
    

    function displayProducts($produtos,$produtos_por_linha){
        for($i = 0;$i < $produtos_por_linha; $i++){
            echo '
                <div class="card mb-4 ml-2" style="width: 25rem;">
                    <img src="'.$produtos[$i]->getImgPath().'" class="card-img-top" alt="...">
                    <div class="card-body">
                        <h5 class="card-title">'.$produtos[$i]->getProductName().'</h5>
                        <p class="card-text" style="display:none;">'.$produtos[$i]->getIndexDesc().'</p>
                        <input type="hidden" name="product_id" value="'.$produtos[$i]->getId().'">
                        <a href="./produto_detalhe.php?id='.$produtos[$i]->getId().'" class="btn btn-outline-primary float-right mr-1">Propor Troca</a>
                    </div>
            </div>';

        }
    }
?>

