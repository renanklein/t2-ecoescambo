<?php
    require_once 'php/produto/produto.php';

    function getRowCount($conn,$limite){
        $result = $conn->query("SELECT COUNT(p.product_id) as linhas from product p JOIN oferta o 
        ON p.product_id = o.produto_ofertado_id WHERE o.status = 'Em aberto';");
        $total = $result->fetch(PDO::FETCH_ASSOC)['linhas'];
        return ceil($total/$limite);
    }


    function getProducts($conn,$pagina,$limite,$userid,$option){
        $result = 0;
        $primeiro = ($pagina - 1)*$limite;
        if($option === 1){
            $result = $conn->prepare("SELECT * FROM product WHERE user_userid = :id");
            $result->bindParam(':id',$userid,PDO::PARAM_INT);
        }else if($option === 2){
            $result = $conn->prepare("SELECT * FROM product WHERE user_userid = :id LIMIT :primeiro,:limite");
            $result->bindParam(':id',$userid,PDO::PARAM_INT);
            $result->bindParam(':primeiro',$primeiro,PDO::PARAM_INT);
            $result->bindParam(':limite',$limite,PDO::PARAM_INT);
        }else{
            $result = $conn->prepare(
            "SELECT p.*,o.status FROM product p JOIN oferta o on o.produto_ofertado_id = p.product_id WHERE p.user_userid != :id and o.status = 'Em aberto' 
            ORDER BY p.product_id LIMIT :primeiro,:limite;"
            );
            $result->bindParam(':id',$userid,PDO::PARAM_INT);
            $result->bindParam(':primeiro',$primeiro,PDO::PARAM_INT);
            $result->bindParam(':limite',$limite,PDO::PARAM_INT);
        }
        $result->execute();
       
        //($pagina - 1)*$limite obtem o primeiro elemento da proxima página a ser exibido
        
        $produtosDB = $result->fetchAll(PDO::FETCH_ASSOC);

        $produtos = [];

        foreach($produtosDB as $i => $produto){
            $id = $produto['product_id'];
            $img = $produto['img_path'];
            $index_desc = $produto['index_description'];
            $detalhes = $produto['detail_description'];
            $name =  $produto['product_name'];
            $produtos[$i] = new Produto($id,$img,$index_desc,$detalhes,$name);
            //echo '<img src="'.$produtos[$i]->getImgPath().'">';
        }

        return $produtos;
    }

    function getProduct($conn,$id){
        $result = $conn->query("SELECT * FROM product WHERE product_id = $id");
        $produto = $result->fetch(PDO::FETCH_ASSOC);

        return new Produto($produto['product_id'],$produto['img_path'],$produto['index_description']
        ,$produto['detail_description'],$produto['product_name']);
    }


    
