<?php
    require_once '../connection.php';
    $user_id = intval(filter_input(INPUT_GET,"user_id",FILTER_SANITIZE_STRING));
    $query = $conn->prepare("SELECT produto_ofertado_id,produto_proposto_id FROM oferta WHERE interessado_id = :id 
    and produto_proposto_id is not null and status = 'Em aberto'");
    $query->bindParam(":id",$user_id,PDO::PARAM_INT);
    $query->execute();
    $produtos_ids = $query->fetchAll(PDO::FETCH_ASSOC);
    $par_propostas = [];
    foreach ($produtos_ids as $i => $produto_id) {
        $query = $conn->prepare("SELECT * FROM product  WHERE product_id = :id1 OR product_id = :id2");
        $query->bindParam(':id1',$produto_id['produto_ofertado_id'],PDO::PARAM_INT);
        $query->bindParam(':id2',$produto_id['produto_proposto_id'],PDO::PARAM_INT);
        $query->execute();
        $proposta = $query->fetchAll(PDO::FETCH_ASSOC);
        foreach ($proposta as $k =>$produto) {
            $par_propostas[$k] = $produto;
        }
    }
    echo(json_encode($par_propostas));
