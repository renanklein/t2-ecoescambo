<?php
    class Produto{
        private $id_produto;
        private $imgPath;
        private $indexDesc;
        private $product_name;
        private $detalhes;
        private $status;

        public function __construct($id,$img,$index,$detalhesDesc,$name){
            $this->_id_produto = $id;
            $this->_imgPath = $img;
            $this->_indexDesc = $index;
            $this->_detalhes = $detalhesDesc;
            $this->_product_name = $name;
        }

        public function getId(){
            return $this->_id_produto;
        }
        public function getImgPath(){
            return $this->_imgPath;
        }

        public function getProductName(){
            return $this->_product_name;
        }
   
        public function getIndexDesc(){
            return $this->_indexDesc;
        }
   
        public function getDetalhes(){
            return $this->_detalhes;
        }
        public function getStatus(){
            return $this->_status;
        }
        public function setStatus($status){
            $this->_status = $status;
        }
    }
?>