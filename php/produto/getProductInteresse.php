<?php
    require_once '../connection.php';
    session_start();
    $pagina = filter_input(INPUT_GET,'pagina',FILTER_SANITIZE_STRING) ?
    filter_input(INPUT_GET,'pagina',FILTER_SANITIZE_STRING): 1;
    $username = $_SESSION['user'];
    $query = $conn->prepare("SELECT userid FROM user WHERE username = :user");
    $query->bindParam(':user',$username,PDO::PARAM_STR,60);
    $query->execute();
    $user = $query->fetch(PDO::FETCH_ASSOC);

    $userid = $user['userid'];

    $query = $conn->prepare(
        "SELECT oferta_id,produto_ofertado_id,interessado_id 
        FROM oferta WHERE ofertante_id = :id and interessado_id is not NULL
         and status = 'Em aberto' ORDER BY produto_ofertado_id;"
    );

    $query->bindParam(':id',$userid,PDO::PARAM_INT);
    //$query->bindParam(':pagina',$pagina,PDO::PARAM_INT);
    $query->execute();
    $interesses = $query->fetchAll(PDO::FETCH_ASSOC);

    if(count($interesses) === 0){
        echo(json_encode($interesses));
    }else{
        $produtos = [];

        foreach($interesses as $i => $product){
            //Obtem os produtos com interesse e o nome do interessado
            $query = $conn->prepare(
                "SELECT p.*,u.username,o.oferta_id FROM product p,user u,oferta o WHERE p.product_id = :id 
                and u.userid = :uid and o.interessado_id = :iid  and o.produto_ofertado_id = :piud;"
            );
            $query->bindParam(':id',$product["produto_ofertado_id"],PDO::PARAM_INT);
            $query->bindParam(':uid',$product['interessado_id'],PDO::PARAM_INT);
            $query->bindParam(':iid',$product['interessado_id'],PDO::PARAM_INT);
            $query->bindParam(':piud',$product["produto_ofertado_id"],PDO::PARAM_INT);
            $query->execute();
            $item = $query->fetch(PDO::FETCH_ASSOC);
            $produtos[$i] = $item;
        }
        echo(json_encode($produtos));   
    }


