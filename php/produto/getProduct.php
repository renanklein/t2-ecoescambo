<?php
    require_once '../connection.php';
    $produtos_id = json_decode($_GET["produtos_id"]);
    $produtos = [];
    foreach ($produtos_id as $i => $produto_id) {
        $query = $conn->prepare("SELECT * FROM product WHERE product_id = :id");
        $query->bindParam(':id',intval($produto_id),PDO::PARAM_INT);
        $query->execute();
        $produto = $query->fetch(PDO::FETCH_ASSOC);
        $produtos[$i] = $produto;
    }

    echo(json_encode($produtos));