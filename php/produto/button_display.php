<?php 
    require 'php/connection.php';
    $pagina = filter_input(INPUT_GET,'pagina',FILTER_SANITIZE_NUMBER_INT) !== null ?
    filter_input(INPUT_GET,'pagina',FILTER_SANITIZE_NUMBER_INT) : 1;

    $ultima_pagina = getRowCount($conn,10);
    
    $anterior = $pagina == 1 ? 
        '<li class="page-item disabled"><a href="?pagina='.($pagina - 1).'" class="page-link">&laquo;</a></li>':
        '<li class="page-item "><a href="?pagina='.($pagina - 1).'" class="page-link">&laquo;</a></li>';
    echo $anterior;

    for($i = 1; $i <= $ultima_pagina; $i++){
        $link;
        if($pagina == $i){
            $link ='<li class="page-item active"><a href="?pagina='.$i.'" class="page-link">'.$i.'</a></li>';
        }else{
            $link ='<li class="page-item"><a href="?pagina='.$i.'" class="page-link">'.$i.'</a></li>';
        }
        echo $link;
    }

    $posterior = $pagina == $ultima_pagina ?
        '<li class="page-item disabled"><a href="?pagina='.($pagina + 1).'" class="page-link">&raquo;</a></li>':
        '<li class="page-item"><a href="?pagina='.($pagina + 1).'" class="page-link">&raquo;</a></li>';

    echo $posterior;
?>     
