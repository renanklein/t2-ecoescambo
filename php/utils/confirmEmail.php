<?php
    require_once '../connection.php';
    $ativo = true;
    $username = filter_input(INPUT_GET,"user",FILTER_SANITIZE_STRING);
    $query = $conn->prepare("UPDATE user SET ativo = :ativo WHERE username = :user");
    $query->bindParam(':user',$username,PDO::PARAM_STR,60);
    $query->bindParam(':ativo',$ativo,PDO::PARAM_BOOL);
    $query->execute();

    $_SESSION['success'] = '<div class="alert alert-success m-5"> Conta ativada com sucesso !</div>';

    header("location: ../../indexecoescambo.php");
