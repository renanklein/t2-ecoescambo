<?php
    session_start();
    if(!isset($_SESSION["user"])){
        $_SESSION["msg"] = '<p class="alert alert-danger">Acesso Negado! Se autentique no sistema para ter acesso ao conteúdo</p>';
        header('Location: login.php');
    }