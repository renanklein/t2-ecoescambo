<?php
    use PHPMailer\PHPMailer\PHPMailer;
    use PHPMailer\PHPMailer\Exception;
    
    require $_SERVER['DOCUMENT_ROOT'] . '/mail/Exception.php';
    require $_SERVER['DOCUMENT_ROOT'] . '/mail/PHPMailer.php';
    require $_SERVER['DOCUMENT_ROOT'] . '/mail/SMTP.php';

    function ifUserExistsDB($conn){
        $result = $conn->query("SELECT username,email,tel from user;");
        return $result;
    }
    function userAlreadyExists(){
        $_SESSION["msg"] = '<div class="alert alert-danger m-5">Erro! Usuário já existe</div>';
        header('Location: ../cadastro_user.php');
    }
    function emailAlreadyExists(){
        $_SESSION["msg"] = '<div class="alert alert-danger m-5">Erro! E-mail já cadastrado</div>';
        header('Location: ../cadastro_user.php');
    }
    function phoneAlreadyExists(){
        $_SESSION["msg"] = '<div class="alert alert-danger m-5">Erro! Telefone já cadastrado</div>';
        header('Location: ../cadastro_user.php');
    }
    function userSignUp($user, $password, $email, $tel, $cep,$conn){
        $ativo = false;
        $hash = password_hash($password,PASSWORD_DEFAULT);
        $query = $conn->prepare("INSERT INTO user (username,senha,email,tel,cep,ativo) values (:user,:hashString,:email,:tel,:cep,:ativo);");
        $query->bindParam(':user',$user,PDO::PARAM_STR,60);
        $query->bindParam(':hashString',$hash,PDO::PARAM_STR,255);
        $query->bindParam(':email',$email,PDO::PARAM_STR,200);
        $query->bindParam(':tel',$tel,PDO::PARAM_STR,14);
        $query->bindParam(':cep',$cep,PDO::PARAM_STR,9);
        $query->bindParam(':ativo',$ativo,PDO::PARAM_BOOL);
        
        // Envio de email de confirmacao atraves da biblioteca PHPMailer
        $mail = new PHPMailer();
        $mail->isSMTP();
        $mail->Port = 587;
        $mail->Host = 'smtp.gmail.com';
        $mail->IsHTML(true); 
        $mail->CharSet = "UTF-8";
        $mail->Mailer = 'smtp'; 
        $mail->SMTPSecure = 'tls';
        
        //Origem dos emails de confirmação
        $mail->SMTPAuth = true;
        $mail->Username= "ecoescamboMailer@gmail.com";
        $mail->Password = "TrabalhoLPW";
        $mail->SingleTo = true;
        
        //A menssagem em si
        $url_confirm = "http://sports-av2-internet.000webhostapp.com/EcoEscambo/php/utils/confirmEmail.php?user=$user";
        $url = htmlentities($url_confirm);
        $message = '<h1>Você se cadastrou no sistema Ecoescambo</h1>
        <p> Para ativar sua conta clique no link abaixo
        <a href="'.$url.'">
        Clique aqui para confirmar conta</a>';
        $mail->From = "";
        $mail->FromName = "EcoEscambo inc";
        
        $mail->addAddress($email);
        $titulo = 'Confirmação de email';
        $mail->Subject = $titulo;
        $mail->Body = $message ;
        
        if(!$mail->Send()){
            $_SESSION['msg'] = '<div class="alert alert-danger">Ocorreu um erro ao enviar o email de confirmação: '.$mail->ErrorInfo.'</div>'; 
        }
        else{
            $_SESSION['success'] = '<div class="alert alert-success">Conta cadastrada com sucesso !
            Foi enviado um email de confirmação de cadastro. Você deve acessá-lo para ativar sua conta</div>';
            $query->execute();
        }
        header('Location: ../indexecoescambo.php');  
    }