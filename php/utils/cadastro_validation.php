<?php
    $user = filter_input(INPUT_POST,"username",FILTER_SANITIZE_STRING);
    $password = filter_input(INPUT_POST,"senha",FILTER_SANITIZE_STRING);
    $confirmPass = filter_input(INPUT_POST,"cp",FILTER_SANITIZE_STRING);
    $email = filter_input(INPUT_POST,"email",FILTER_SANITIZE_EMAIL);
    $tel  = filter_input(INPUT_POST,"tel",FILTER_SANITIZE_STRING);
    $cep = filter_input(INPUT_POST,"cep",FILTER_SANITIZE_STRING);


    function validateCadastro($user,$password,$confirmPass,$email,$tel,$cep){
        $validArray = array();
        //usuario
        if(preg_match('/^\w{8,}$/',$user)){
            $validArray[0] = 'is-valid' ;
        }else{
            $validArray[0] = 'is-invalid';
    
        }

        //senha
        if(strlen($password) > 6 ){
            $validArray[1] = 'is-valid';
        } else{
            $validArray[1] = 'is-invalid';
    
        }

        //confirmação de senha
        if($confirmPass === $password){
            $validArray[2] = 'is-valid';
        }else{
            $validArray[2] = 'is-invalid';
    
        }

        //email
        if(filter_var($email,FILTER_SANITIZE_EMAIL)){
            $validArray[3] = 'is-valid';
        }else{
            $validArray[3] = 'is-invalid';
    
        }

        //telefone
        if(preg_match('/^\(?\d{2}\)?\s?\d{5}\-?\d{4}$/',$tel)){
            $validArray[4] = 'is-valid';
        } else{
            $validArray[4] = 'is-invalid';
    
        }

        //CEP
        if(preg_match('/^\d{5}-\d{3}$/',$cep)){
            $validArray[5] = 'is-valid';
        }else{
            $validArray[5] = 'is-invalid';
    
        }
        return $validArray;
    }

    function onValidateError($validArray){
        $_SESSION['erroClass'] = $validArray;
        $_SESSION['msg'] = '<div class="alert alert-danger">Erro! Informações errada, por favor verifique os campos digitados</div>';
        header('Location: ../cadastro_user.php');
    }