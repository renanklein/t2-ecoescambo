<?php
    session_start();
    require_once "connection.php";
    $username = filter_input(INPUT_POST,"user",FILTER_SANITIZE_STRING);
    $pass = filter_input(INPUT_POST,"senha",FILTER_SANITIZE_STRING);

    $result = $conn->prepare("SELECT username, senha, email,ativo from user where username = :user OR email = :email;");
    $result->bindParam(":user",$username,PDO::PARAM_STR,60);
    $result->bindParam(":email",$username,PDO::PARAM_STR,200);
    $result->execute();

    //Validações para o usuário logar ...
    if($result->rowCount() == 0){
        $_SESSION["msg"] = '<p class="alert alert-danger m-5"> Usuário não encontrado! </p>';
        header('Location: ../login.php');
    } else{
        //Sendo informado o username ou email para login, só será recuperado apenas uma linha no banco de dados...
        $userDB = $result->fetch(PDO::FETCH_ASSOC);
        if(!password_verify($pass,$userDB["senha"])){
            $_SESSION["msg"] = '<p class="alert alert-danger m-5"> Senha incorreta !</p>';
            header('Location: ../login.php');
        }
        else if(!$userDB['ativo']){
            $_SESSION['msg'] =  '<p class="alert alert-warning m-5">Conta desativada. Clique no link enviado 
            no e-mail de confirmação para ativar sua conta</p>';
            header("Location: ../indexecoescambo.php");
        }
        else{
            $_SESSION["user"] = $userDB["username"];
            header('Location: ../indexecoescambo.php');
        }
    }  
