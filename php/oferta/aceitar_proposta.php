<?php
    session_start();
    require_once '../connection.php';
    $ofertado = intval(filter_input(INPUT_POST,"oferta",FILTER_SANITIZE_STRING));
    $oferecido = intval(filter_input(INPUT_POST,"interesse",FILTER_SANITIZE_STRING));

    $query = $conn->prepare(
        "UPDATE oferta SET status='Finalizada' WHERE produto_ofertado_id = :id"
    );

    $query->bindParam(":id",$ofertado,PDO::PARAM_INT);
    $query->execute();

    $query = $conn->prepare("UPDATE oferta SET status='Finalizada' WHERE produto_ofertado_id = :id2");
    $query->bindParam(":id2",$oferecido,PDO::PARAM_INT);

    $query->execute();

    $_SESSION["success"] = '<div class="alert alert-success mx-5" style="margin-top:10vh;">Produto foi arrematado com sucesso! Segue abaixo os contatos com o outro interessado</div>';

    header("location: ../../confirmacao_contato.php?ofertado=$ofertado");