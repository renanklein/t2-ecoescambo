<?php
    session_start();
    use PHPMailer\PHPMailer\PHPMailer;
    use PHPMailer\PHPMailer\Exception;
    
    require $_SERVER['DOCUMENT_ROOT'] . '/mail/Exception.php';
    require $_SERVER['DOCUMENT_ROOT'] . '/mail/PHPMailer.php';
    require $_SERVER['DOCUMENT_ROOT'] . '/mail/SMTP.php';
    
    require_once '../connection.php';
    $username = $_SESSION['user'];
    $product_id = filter_input(INPUT_GET,'id',FILTER_SANITIZE_NUMBER_INT);
    $query = $conn->prepare("SELECT userid FROM user WHERE username = :user");
    $query->bindParam(':user',$username,PDO::PARAM_STR,60);
    $query->execute();
    $user = $query->fetch(PDO::FETCH_ASSOC);

    $userid = $user['userid'];

    $query = $conn->prepare("UPDATE oferta SET interessado_id = :interesse WHERE produto_ofertado_id = :id");

    $query->bindParam(':interesse',$userid,PDO::PARAM_INT);
    $query->bindParam(':id',$product_id,PDO::PARAM_INT);

    $query->execute();
    
    //Obtendo o email do usuário que esta ofertando o produto...
    $query = $conn->prepare("SELECT u.email FROM user u JOIN product p on p.user_userid = u.userid WHERE product_id = :id;");
    
    $query->bindParam(':id',$product_id,PDO::PARAM_INT);
    $query->execute();
    $result = $query->fetch(PDO::FETCH_ASSOC);
    $email = $result['email'];
    
    $mail = new PHPMailer();
        $mail->isSMTP();
        $mail->Port = 587;
        $mail->Host = 'smtp.gmail.com';
        $mail->IsHTML(true); 
        $mail->CharSet = "UTF-8";
        $mail->Mailer = 'smtp'; 
        $mail->SMTPSecure = 'tls';
        
        //Origem dos emails de confirmação
        $mail->SMTPAuth = true;
        $mail->Username= "ecoescamboMailer@gmail.com";
        $mail->Password = "TrabalhoLPW";
        $mail->SingleTo = true;
        
        //A menssagem em si
        $message = '<h1>Alguem se interessou por um de seus produtos !</h1>
        <p> Acesse a funcionalidade "Avaliar Interesse" para saber mais ';
        $mail->From = "";
        $mail->FromName = "EcoEscambo inc";
        
        $mail->addAddress($email);
        $titulo = 'Interesse';
        $mail->Subject = $titulo;
        $mail->Body = $message ;
        
        if(!$mail->Send()){
            $_SESSION['msg'] = '<div class="alert alert-danger">Ocorreu um erro ao enviar o email de notificação da operação</div>'; 
        }
    

    header('location: ../../interesse_confirm.php');
