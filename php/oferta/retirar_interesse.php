<?php
    require_once '../connection.php';
    session_start();
    $oferta_id = $_SESSION['oferta_id'];

    $query = $conn->prepare("UPDATE oferta SET interessado_id = NULL WHERE oferta_id = :oferta");
    $query->bindParam(':oferta',$oferta_id,PDO::PARAM_INT);
    $query->execute();
    unset($_SESSION["oferta_id"]);

    $_SESSION["success"] = '<div class="alert alert-warning m-5">Foi retirado o interesse do usuário em seu produto</div>';

    header('location: ../../indexecoescambo.php');