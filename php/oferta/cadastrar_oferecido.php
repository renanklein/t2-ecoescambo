<?php
    session_start();
    use PHPMailer\PHPMailer\PHPMailer;
    use PHPMailer\PHPMailer\Exception;
    
    require $_SERVER['DOCUMENT_ROOT'] . '/mail/Exception.php';
    require $_SERVER['DOCUMENT_ROOT'] . '/mail/PHPMailer.php';
    require $_SERVER['DOCUMENT_ROOT'] . '/mail/SMTP.php';
    
    require_once '../connection.php';
    $oferta_id = $_SESSION['oferta_id'];
    $oferecido_id = intval(filter_input(INPUT_GET,"produto",FILTER_SANITIZE_STRING));

    $query = $conn->prepare("UPDATE oferta SET produto_proposto_id = :produto WHERE oferta_id = :id");
    $query->bindParam(':produto',$oferecido_id,PDO::PARAM_INT);
    $query->bindParam(':id',$oferta_id,PDO::PARAM_INT);
    $query->execute();
    
    $query = $conn->prepare("SELECT u.email FROM user u JOIN product p on u.userid = p.user_userid WHERE p.product_id = :id");
    
    $query->bindParam(':id',$oferecido_id,PDO::PARAM_INT);
    
    $query->execute();
    $result = $query->fetch(PDO::FETCH_ASSOC);
    $email = $result['email'];
    
    $mail = new PHPMailer();
        $mail->isSMTP();
        $mail->Port = 587;
        $mail->Host = 'smtp.gmail.com';
        $mail->IsHTML(true); 
        $mail->CharSet = "UTF-8";
        $mail->Mailer = 'smtp'; 
        $mail->SMTPSecure = 'tls';
        
        //Origem dos emails de confirmação
        $mail->SMTPAuth = true;
        $mail->Username= "ecoescamboMailer@gmail.com";
        $mail->Password = "TrabalhoLPW";
        $mail->SingleTo = true;
        
        //A menssagem em si
        $message = '<h1>O usuário dono do produto em que você se interessou escolheu um outro produto em troca !</h1>
        <p> Acesse a funcionalidade "Avaliar Proposta" aceitar ou rejeitar o produto ! ';
        $mail->From = "";
        $mail->FromName = "EcoEscambo inc";
        
        $mail->addAddress($email);
        $titulo = 'Produto em troca';
        $mail->Subject = $titulo;
        $mail->Body = $message ;
        
        if(!$mail->Send()){
            $_SESSION['msg'] = '<div class="alert alert-danger">Ocorreu um erro ao enviar o email de notificação da operação</div>'; 
        }
    

    $_SESSION['success'] = '<div class="alert alert-success">O registro da proposta de troca foi realizado com sucesso!</div>';

    unset($_SESSION["oferta_id"]);

    header('location:../../indexecoescambo.php');