<?php
    session_start();
    use PHPMailer\PHPMailer\PHPMailer;
    use PHPMailer\PHPMailer\Exception;
    
    require $_SERVER['DOCUMENT_ROOT'] . '/mail/Exception.php';
    require $_SERVER['DOCUMENT_ROOT'] . '/mail/PHPMailer.php';
    require $_SERVER['DOCUMENT_ROOT'] . '/mail/SMTP.php';
    
    require_once '../connection.php';
    $ofertado = intval(filter_input(INPUT_POST,"oferta",FILTER_SANITIZE_STRING));
    $oferecido = intval(filter_input(INPUT_POST,"interesse",FILTER_SANITIZE_STRING));

    $query = $conn->prepare(
        "UPDATE oferta SET interessado_id = NULL, produto_proposto_id = NULL WHERE produto_ofertado_id = :id1 
        and produto_proposto_id = :id2"
    );
    
    $query->bindParam(":id1",$ofertado,PDO::PARAM_INT);
    $query->bindParam(":id2",$oferecido,PDO::PARAM_INT);

    $query->execute();
    
    $query  = $conn->prepare("SELECT u.email FROM user u JOIN product p ON p.user_userid = u.userid WHERE p.product_id = :id");
    
    $query->bindParam(":id",$ofertado,PDO::PARAM_INT);
    $query->execute();
    $result = $query->fetch(PDO::FETCH_ASSOC);
    
    $email = $result['email'];
    
    $mail = new PHPMailer();
        $mail->isSMTP();
        $mail->Port = 587;
        $mail->Host = 'smtp.gmail.com';
        $mail->IsHTML(true); 
        $mail->CharSet = "UTF-8";
        $mail->Mailer = 'smtp'; 
        $mail->SMTPSecure = 'tls';
        
        //Origem dos emails de confirmação
        $mail->SMTPAuth = true;
        $mail->Username= "ecoescamboMailer@gmail.com";
        $mail->Password = "TrabalhoLPW";
        $mail->SingleTo = true;
        
        //A menssagem em si
        $message = '<h1>Que Pena! A sua proposta foi rejeitada !</h1>
        <p> Acesso o catálogo de produtos do EcoEscambo arrematar outros produto !</p> ';
        $mail->From = "";
        $mail->FromName = "EcoEscambo inc";
        
        $mail->addAddress($email);
        $titulo = 'Proposta';
        $mail->Subject = $titulo;
        $mail->Body = $message ;
        
        if(!$mail->Send()){
            $_SESSION['msg'] = '<div class="alert alert-danger">Ocorreu um erro ao enviar o email notificando o usuário da operação</div>'; 
        }
    

    $_SESSION["success"] = '<div class="alert alert-success m-4">Produto rejeitado com sucesso</div>';

    header("location: ../../indexecoescambo.php");